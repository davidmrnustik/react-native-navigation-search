import React from 'react';
import { View, Text } from 'react-native';
import { FontAwesome, Ionicons } from '@expo/vector-icons';

export default function ProductVerticalTabBar({ productVerticals }) {
  return (
    productVerticals && productVerticals.map((item, index) => (
      <View key={index}>
        <Text>{item.title}</Text>
        <FontAwesome name={item.icon} size={30} color={item.color} />
      </View>
    ))
  )
}