import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ScrollView } from 'react-native';

const SearchMoreResults = ({ navigation }) => (
  <View style={styles.container}>
    <ScrollView keyboardShouldPersistTaps="always">
      <Text>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse in aliquam odio. In libero felis, dignissim id ante auctor, dapibus tincidunt ante. Quisque finibus semper libero, sed egestas tellus iaculis sed. Aliquam sed bibendum dui. Pellentesque congue sagittis est, et maximus felis venenatis eget. Maecenas mollis, lorem non porta blandit, erat ligula convallis felis, nec viverra turpis neque ac turpis. Cras sed velit sit amet diam semper eleifend. Curabitur scelerisque tortor ut purus finibus porttitor. Aliquam aliquet pharetra ligula volutpat lobortis.

      Aenean ultricies elit quis consequat molestie. Etiam a malesuada libero. In justo velit, cursus et leo quis, facilisis blandit nunc. Sed varius, lorem non consequat mattis, mauris mi mollis dui, non dictum enim libero nec ligula. Sed facilisis volutpat pretium. Vestibulum volutpat justo id lacus vehicula, at ultricies urna tempor. Morbi sit amet volutpat lorem. In hac habitasse platea dictumst. Aliquam vehicula sagittis tellus. Ut venenatis, lectus sit amet sagittis malesuada, nulla neque semper purus, viverra maximus nulla ante id ligula.
      </Text>
      <Text style={{ paddingTop: 20 }}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse in aliquam odio. In libero felis, dignissim id ante auctor, dapibus tincidunt ante. Quisque finibus semper libero, sed egestas tellus iaculis sed. Aliquam sed bibendum dui. Pellentesque congue sagittis est, et maximus felis venenatis eget. Maecenas mollis, lorem non porta blandit, erat ligula convallis felis, nec viverra turpis neque ac turpis. Cras sed velit sit amet diam semper eleifend. Curabitur scelerisque tortor ut purus finibus porttitor. Aliquam aliquet pharetra ligula volutpat lobortis.

        Aenean ultricies elit quis consequat molestie. Etiam a malesuada libero. In justo velit, cursus et leo quis, facilisis blandit nunc. Sed varius, lorem non consequat mattis, mauris mi mollis dui, non dictum enim libero nec ligula. Sed facilisis volutpat pretium. Vestibulum volutpat justo id lacus vehicula, at ultricies urna tempor. Morbi sit amet volutpat lorem. In hac habitasse platea dictumst. Aliquam vehicula sagittis tellus. Ut venenatis, lectus sit amet sagittis malesuada, nulla neque semper purus, viverra maximus nulla ante id ligula.
        Vestibulum volutpat justo id lacus vehicula, at ultricies urna tempor. Morbi sit amet volutpat lorem. In hac habitasse platea dictumst. Aliquam vehicula sagittis tellus. Ut venenatis, lectus sit amet sagittis malesuada, nulla neque semper purus, viverra maximus nulla ante id ligula.
      </Text>
      <Text style={{ paddingTop: 20 }}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse in aliquam odio. In libero felis, dignissim id ante auctor, dapibus tincidunt ante. Quisque finibus semper libero, sed egestas tellus iaculis sed. Aliquam sed bibendum dui. Pellentesque congue sagittis est, et maximus felis venenatis eget. Maecenas mollis, lorem non porta blandit, erat ligula convallis felis, nec viverra turpis neque ac turpis. Cras sed velit sit amet diam semper eleifend. Curabitur scelerisque tortor ut purus finibus porttitor. Aliquam aliquet pharetra ligula volutpat lobortis.
      </Text>
      <Text style={{ paddingTop: 20 }}>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse in aliquam odio. In libero felis, dignissim id ante auctor, dapibus tincidunt ante. Quisque finibus semper libero, sed egestas tellus iaculis sed. Aliquam sed bibendum dui. Pellentesque congue sagittis est, et maximus felis venenatis eget. Maecenas mollis, lorem non porta blandit, erat ligula convallis felis, nec viverra turpis neque ac turpis. Cras sed velit sit amet diam semper eleifend. Curabitur scelerisque tortor ut purus finibus porttitor. Aliquam aliquet pharetra ligula volutpat lobortis.

        Aenean ultricies elit quis consequat molestie. Etiam a malesuada libero. In justo velit, cursus et leo quis, facilisis blandit nunc. Sed varius, lorem non consequat mattis, mauris mi mollis dui, non dictum enim libero nec ligula. Sed facilisis volutpat pretium. Vestibulum volutpat justo id lacus vehicula, at ultricies urna tempor. Morbi sit amet volutpat lorem. In hac habitasse platea dictumst. Aliquam vehicula sagittis tellus. Ut venenatis, lectus sit amet sagittis malesuada, nulla neque semper purus, viverra maximus nulla ante id ligula.
        Vestibulum volutpat justo id lacus vehicula, at ultricies urna tempor. Morbi sit amet volutpat lorem. In hac habitasse platea dictumst. Aliquam vehicula sagittis tellus. Ut venenatis, lectus sit amet sagittis malesuada, nulla neque semper purus, viverra maximus nulla ante id ligula.
      </Text>
      <TouchableOpacity style={{ marginTop: 20, paddingBottom: 20 }} onPress={() => navigation.navigate('SearchHome')}>
        <Text style={{ color: 'blue'}}>Go to Search Home</Text>
      </TouchableOpacity>
    </ScrollView>
  </View>
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default SearchMoreResults;