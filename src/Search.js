import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, ScrollView, Animated, TextInput } from 'react-native';
import algoliasearch from 'algoliasearch/lite';
import { InstantSearch } from 'react-instantsearch-dom';
import ScrollViewKeyboardHandler from './ScrollViewKeyboardHandler';
import InfiniteHits from './InfiniteHits';
import SearchBox from './SearchBox';

const searchClient = algoliasearch(
  'latency',
  '6be0576ff61c053d5f9a3225e2a90f76'
);

class SearchNavigator extends React.Component {
  root = {
    Root: View,
    props: {
      style: {
        flex: 1,
      },
    },
  };

  render() {
    const { navigation } = this.props;
    return (
      <ScrollViewKeyboardHandler>
        {/* https://github.com/GeekyAnts/NativeBase/blob/master/src/basic/Container.js#L15 */}
        <View style={styles.container} accessibilityLabel="This simulates Container from native-base">
          {/* https://github.com/algolia/react-instantsearch/blob/master/packages/react-instantsearch-native/src/widgets/InstantSearch.js#L6 */}
          <InstantSearch
            searchClient={searchClient}
            indexName="instant_search"
            root={this.root}
          >
            <SearchBox />
            {/*<InfiniteHits />*/}
            {/*<View style={styles.container} accessibilityLabel="This simulates InstantView from react-instantsearch">*/}
            <ScrollView keyboardShouldPersistTaps="always">
              <Text>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse in aliquam odio. In libero felis, dignissim id ante auctor, dapibus tincidunt ante. Quisque finibus semper libero, sed egestas tellus iaculis sed. Aliquam sed bibendum dui. Pellentesque congue sagittis est, et maximus felis venenatis eget. Maecenas mollis, lorem non porta blandit, erat ligula convallis felis, nec viverra turpis neque ac turpis. Cras sed velit sit amet diam semper eleifend. Curabitur scelerisque tortor ut purus finibus porttitor. Aliquam aliquet pharetra ligula volutpat lobortis.

                Aenean ultricies elit quis consequat molestie. Etiam a malesuada libero. In justo velit, cursus et leo quis, facilisis blandit nunc. Sed varius, lorem non consequat mattis, mauris mi mollis dui, non dictum enim libero nec ligula. Sed facilisis volutpat pretium. Vestibulum volutpat justo id lacus vehicula, at ultricies urna tempor. Morbi sit amet volutpat lorem. In hac habitasse platea dictumst. Aliquam vehicula sagittis tellus. Ut venenatis, lectus sit amet sagittis malesuada, nulla neque semper purus, viverra maximus nulla ante id ligula.
                </Text>
                <Text style={{ paddingTop: 20 }}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse in aliquam odio. In libero felis, dignissim id ante auctor, dapibus tincidunt ante. Quisque finibus semper libero, sed egestas tellus iaculis sed. Aliquam sed bibendum dui. Pellentesque congue sagittis est, et maximus felis venenatis eget. Maecenas mollis, lorem non porta blandit, erat ligula convallis felis, nec viverra turpis neque ac turpis. Cras sed velit sit amet diam semper eleifend. Curabitur scelerisque tortor ut purus finibus porttitor. Aliquam aliquet pharetra ligula volutpat lobortis.

                Aenean ultricies elit quis consequat molestie. Etiam a malesuada libero. In justo velit, cursus et leo quis, facilisis blandit nunc. Sed varius, lorem non consequat mattis, mauris mi mollis dui, non dictum enim libero nec ligula. Sed facilisis volutpat pretium. Vestibulum volutpat justo id lacus vehicula, at ultricies urna tempor. Morbi sit amet volutpat lorem. In hac habitasse platea dictumst. Aliquam vehicula sagittis tellus. Ut venenatis, lectus sit amet sagittis malesuada, nulla neque semper purus, viverra maximus nulla ante id ligula.
                Vestibulum volutpat justo id lacus vehicula, at ultricies urna tempor. Morbi sit amet volutpat lorem. In hac habitasse platea dictumst. Aliquam vehicula sagittis tellus. Ut venenatis, lectus sit amet sagittis malesuada, nulla neque semper purus, viverra maximus nulla ante id ligula.
              </Text>
              <Text style={{ paddingTop: 20 }}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse in aliquam odio. In libero felis, dignissim id ante auctor, dapibus tincidunt ante. Quisque finibus semper libero, sed egestas tellus iaculis sed. Aliquam sed bibendum dui. Pellentesque congue sagittis est, et maximus felis venenatis eget. Maecenas mollis, lorem non porta blandit, erat ligula convallis felis, nec viverra turpis neque ac turpis. Cras sed velit sit amet diam semper eleifend. Curabitur scelerisque tortor ut purus finibus porttitor. Aliquam aliquet pharetra ligula volutpat lobortis.
              </Text>
              <TouchableOpacity style={{ marginTop: 20, paddingBottom: 20 }} onPress={() => navigation.navigate('SearchMoreResults')}>
                <Text style={{ color: 'blue'}}>Go to Search More Results</Text>
              </TouchableOpacity>
            </ScrollView>            
            {/*</View>*/}
          </InstantSearch>
        </View>
      </ScrollViewKeyboardHandler>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default SearchNavigator;