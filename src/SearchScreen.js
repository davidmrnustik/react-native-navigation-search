import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import Search from './Search';
import SearchMoreResults from './SearchMoreResults';

const SearchScreen = createStackNavigator(
  {
    'SearchHome': {
      screen: Search,
      navigationOptions: {
        header: null,
        gesturesEnabled: false,
      }
    },
    'SearchMoreResults': {
      screen: SearchMoreResults,
      navigationOptions: {
      title: 'Search - More results'
      }
    }
  },
  {
    initialRouteName: 'SearchHome',
    headerMode: 'screen',
  }
)

export default SearchScreen;