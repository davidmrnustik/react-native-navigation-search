import React from 'react';
import { Keyboard, Platform, TouchableWithoutFeedback } from 'react-native';

type ScrollViewKeyboardHandlerType = {
    children: React$Element<*>[] | React$Element<*>,
};

const ScrollViewKeyboardHandler = ({ children }: ScrollViewKeyboardHandlerType) => {
    if (Platform.OS === 'ios') {
        return <React.Fragment>{children}</React.Fragment>;
    }

    return <TouchableWithoutFeedback onPress={Keyboard.dismiss}>{children}</TouchableWithoutFeedback>;
};

export default ScrollViewKeyboardHandler;
