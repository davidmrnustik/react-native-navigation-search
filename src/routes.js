import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { createAppContainer, createBottomTabNavigator } from 'react-navigation';
import { FontAwesome, Ionicons } from '@expo/vector-icons';
import StackNavigationApp from './StackNavigationApp';
import ProductVerticalTabBar from './ProductVerticalTabBarContainer';
import SearchScreen from './SearchScreen';

export const Home = ({ navigation }) => (
  <View style={styles.container}>
    <Text>This is the Home view</Text>
    <TouchableOpacity onPress={() => navigation.navigate('AppContainer')}>
      <Text style={{ color: 'blue'}}>Press here for the Dashboard</Text>
    </TouchableOpacity>
  </View>
);

const Dashboard = ({ navigation }) => (
  <View style={styles.container}>
    <Text>This is the Dashboard</Text>
    <TouchableOpacity onPress={() => navigation.navigate('Home')}>
      <Text style={{ color: 'blue'}}>Back to Home</Text>
    </TouchableOpacity>
  </View>
);

const tab = props => <ProductVerticalTabBar {...props} />;

const routeMap = {
  Home: {
    screen: Home,
  },
  AppContainer: {
    screen: createBottomTabNavigator(
      {
        'AppContainer': {
          screen: Dashboard,
          navigationOptions: {
            tabBarIcon: () => <FontAwesome name="home" size={30} color="black" />
          }
        },
        'Search': {
            screen: SearchScreen,
            navigationOptions: {
            tabBarIcon: () => <FontAwesome name="search" size={30} color="black" />
          }
        },
      },
      {
        defaultNavigationOptions: () => ({
          // tabBarComponent: tab,
          tabBarOptions: {
            style: {
              height: 100
            }
          }
        }),
      },
    ),
  },
}

const AppNavigationContainer = createAppContainer(StackNavigationApp(routeMap));

export default AppNavigationContainer;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
