import React from 'react';
import ProductVerticalTabBar from './ProductVerticalTabBar';

const props = {
  productVerticals: [
    {
    id: 'first',
    icon: 'home',
    title: 'AppContainer',
    tabName: 'First',
    color: 'black'
   },
   {
    id: 'second',
    icon: 'search',
    title: 'Search',
    tabName: 'Second',
    color: 'blue'
   },
  ]
}

const ProductVerticalTabBarContainer = () => (
  <ProductVerticalTabBar {...props} />
)

export default ProductVerticalTabBarContainer;
