import React from 'react';
import { StyleSheet, View, TextInput, TouchableOpacity, Animated, Text } from 'react-native';
import PropTypes from 'prop-types';
import { connectSearchBox } from 'react-instantsearch-native';

const styles = StyleSheet.create({
  container: {
    padding: 16,
    backgroundColor: 'grey',
  },
  input: {
    height: 48,
    padding: 12,
    fontSize: 16,
    backgroundColor: '#fff',
    borderRadius: 4,
    borderWidth: 1,
    borderColor: '#ddd',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    shadowRadius: 2,
  }
});

class SearchBox extends React.Component {
  state = {
    anim: new Animated.Value(100)
  }

  setSearchBarHeight(height) {
    const { anim } = this.state;
    Animated.timing(
      anim, {
        toValue: height,
        duration: 200,
      }
    ).start()
  }

  render() {
    const { currentRefinement, refine } = this.props;
    const { anim, text } = this.state;

    return (
      <Animated.View style={{ ...styles.container, height: anim }} accessibilityLabel="This simulates SearchBar">
        <TextInput
          style={styles.input}
          onChangeText={value => refine(value)}
          value={currentRefinement}
          placeholder=""
        />
        <TouchableOpacity onPress={() => this.setSearchBarHeight(150)}>
          <Text>Make bigger</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => this.setSearchBarHeight(100)}>
          <Text>Make smaller</Text>
        </TouchableOpacity> 
      </Animated.View>
    )
  }
}

SearchBox.propTypes = {
  currentRefinement: PropTypes.string.isRequired,
  refine: PropTypes.func.isRequired,
};

export default connectSearchBox(SearchBox);
