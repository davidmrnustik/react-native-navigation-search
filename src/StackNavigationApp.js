import { createStackNavigator } from 'react-navigation';

const stackNavigatorConfig = {
    initialRouteName: 'Home',
    defaultNavigationOptions: {
        header: null,
        gesturesEnabled: false,
    },
    cardStyle: {
        backgroundColor: '#ffffff',
        shadowOpacity: 0,
        opacity: 1,
    },
};

const StackNavigationApp = (routeMap) =>
    createStackNavigator({ ...routeMap }, stackNavigatorConfig);

export default StackNavigationApp;